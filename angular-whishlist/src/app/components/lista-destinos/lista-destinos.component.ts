import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { cdestinoviaje } from './../../models/destinoviaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './../../models/destinos-viajes-state.model';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<cdestinoviaje>;
	updates: string[];
  constructor(public destinosApiClient: DestinosApiClient,private store: Store<AppState>) { 
  this.onItemAdded = new EventEmitter();
  this.updates= [];
  this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if(d != null){
        this.updates.push('se ha elegido a '+ d.nombre);
      }
    });
  }

  ngOnInit(): void {
  }
  agregado(d: cdestinoviaje){
  this.destinosApiClient.add(d);
  this.onItemAdded.emit(d);
  this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e:cdestinoviaje) {
    this.destinosApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

}
