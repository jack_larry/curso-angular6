import { Component, OnInit, Input,HostBinding, EventEmitter, Output} from '@angular/core';
import { cdestinoviaje } from './../../models/destinoviaje.model';
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
@Input() destino: cdestinoviaje;
@Input() position: number;
@HostBinding('attr.class') cssclass= 'col-md-4';
@Output() clicked: EventEmitter<cdestinoviaje>;

constructor() {
  this.clicked= new EventEmitter();
}
  ngOnInit(): void {
  }
  ir(){
    this.clicked.emit(this.destino)
    return false;
  }
}
