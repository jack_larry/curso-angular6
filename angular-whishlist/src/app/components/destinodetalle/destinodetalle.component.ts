import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { cdestinoviaje } from './../../models/destinoviaje.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-destinodetalle',
  templateUrl: './destinodetalle.component.html',
  styleUrls: ['./destinodetalle.component.css'],
  providers: [DestinosApiClient]
})
export class DestinodetalleComponent implements OnInit {
  destino: cdestinoviaje
  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
