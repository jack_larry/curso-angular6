import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinodetalleComponent } from './destinodetalle.component';

describe('DestinodetalleComponent', () => {
  let component: DestinodetalleComponent;
  let fixture: ComponentFixture<DestinodetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinodetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinodetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
