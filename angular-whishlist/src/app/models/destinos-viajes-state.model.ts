import {Injectable} from '@angular/core';
import {Action} from '@ngrx/store';
import {Actions,Effect,ofType} from '@ngrx/effects';
import {Observable,of} from 'rxjs';
import {map} from 'rxjs/operators';
import {cdestinoviaje} from './destinoviaje.model';


export interface DestinoViajeState{
    items: cdestinoviaje[];
    loading: boolean;
    favorito: cdestinoviaje;
}

export const intializableDestinosViajesState = function() {
    return{
        items: [],
        loading: false,
        favorito: null
    };
}

export enum DestinoViajesActionTypes {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito'
}
export class NuevoDestinoAction implements Action {
    type= DestinoViajesActionTypes.NUEVO_DESTINO;
    constructor(public destino: cdestinoviaje){}
}
export class ElegidoFavoritoAction implements Action {
    type= DestinoViajesActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: cdestinoviaje){}
}
export type DestinoViajeActions = NuevoDestinoAction | ElegidoFavoritoAction;

export function reducerDestinosViajes(
    state: DestinoViajeState,
    action: DestinoViajeActions
): DestinoViajeState {
    switch(action.type){
        case DestinoViajesActionTypes.NUEVO_DESTINO: {
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinoViajesActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => x.setSelected(false));
            let fav: cdestinoviaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav
            };
        }
    }
    return state;
}

@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );
    constructor(private actions$: Actions) {}
}