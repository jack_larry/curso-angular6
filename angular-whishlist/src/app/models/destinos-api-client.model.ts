import { cdestinoviaje } from './destinoviaje.model';
import {Subject, BehaviorSubject} from 'rxjs';
export class DestinosApiClient {
    destinos: cdestinoviaje[];
    current: Subject<cdestinoviaje>=new BehaviorSubject<cdestinoviaje>(null);
  constructor(){
      this.destinos= [];
  }
  add(d:cdestinoviaje){
      this.destinos.push(d);
  }
  getAll():cdestinoviaje[]{
      return this.destinos;
  }
  getById(id:string):cdestinoviaje{
      return this.destinos.filter(function(d)
      {return d.isSelected.toString() === id;})[0];
  }
  elegir(d: cdestinoviaje) {
      this.destinos.forEach(x => x.setSelected(false));
      d.setSelected(true);
      this.current.next(d);
  }
  subscribeOnChange(fn){
      this.current.subscribe(fn);
  }
}